package com.example.weatherapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.weatherapp.model.Weather;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс который помогает управлять базой данных и контролировать версии базы.
 *
 * <p>В классе переопределын методы onCreate, onUpgrade и
 * необязательно onOpen, и этот класс позаботится об открытии базы данных
 * если он существует, создать его, если его нет, и обновить его по мере необходимости.
 * Транзакции используются для того, чтобы убедиться, что база данных всегда находится в разумном состоянии.
 * </p>
 */

public class DataHelper extends SQLiteOpenHelper {

    public static final String DATABASE = "weatherdata.db";
    private static final int DB_VERSION = 3;

    public DataHelper(Context context) {
        super(context, DATABASE, null, DB_VERSION);
    }

    /**
     * Переопределяет родительский метод onCreate.
     *
     * <p>Вызывается, когда база данных нуждается в обновлении. Реализация
     * следует использовать этот метод для удаления таблиц, добавления таблиц или делать что-либо еще.
     * необходимо перейти на новую версию схемы.
     *
     * @param db ссылка на объект базы данных.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        DataTable.onCreate(db);
    }

    /**
     * Переопределяет родительский метод onUpgrade.
     * <p>
     * Вызывается, когда база данных должна быть понижена. Это строго похоже на
     * метод onUpgrade, но вызывается всякий раз, когда текущая версия новее запрошенной.
     * Однако этот метод не является абстрактным, поэтому он не является обязательным для клиента.
     * реализовать его. Если он не будет переопределен, реализация по умолчанию отклонит понижение рейтинга и
     * вызывает исключение SQLiteException.
     * </p>
     *
     * @param db         ссылка на объект базы данных.
     * @param oldVersion ссылка на старую версию базу данных.
     * @param newVersion ссылка на новую версию базы данных.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        DataTable.onUpgrade(db, oldVersion, newVersion);
    }

    /**
     * Добавляет новую строчку в таблицу базы данных, принимая параметром объект класса Weather.
     *
     * @param weather ссылка на локальный файл.
     */

    public void insertData(Weather weather) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataTable.DB_COLUMN_CITY, weather.getCityName());
        values.put(DataTable.DB_COLUMN_COUNTRY, weather.getCountry());
        database.insertWithOnConflict(DataTable.DB_TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        database.close();
    }

    /**
     * Удаляет строчку по названию города, принимая его в качестве параметра.
     *
     * @param city ссылка на локальный файл.
     */

    public void deleteData(String city) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(DataTable.DB_TABLE, DataTable.DB_COLUMN_CITY + " = ?", new String[]{city});
        database.close();
    }

    /**
     * Получает ArrayList с данными из таблицы базы данных.
     *
     * <p> Возможно применить сортировку по алфавиту если добавить в качестве параметра rawQuery
     * " ORDER BY " + DataTable.DB_COLUMN_CITY + ", " + DataTable.DB_COLUMN_COUNTRY.
     * </p>
     *
     * @return возвращает ArrayList с объектами класса Weather.
     */
    public ArrayList<Weather> getAllWeathers() {
        ArrayList<Weather> weathersList = new ArrayList<>();
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("select * from " + DataTable.DB_TABLE
                , null);
        while (cursor.moveToNext()) {
            int indexCity = cursor.getColumnIndex(DataTable.DB_COLUMN_CITY);
            int indexCountry = cursor.getColumnIndex(DataTable.DB_COLUMN_COUNTRY);
            weathersList.add(new Weather(cursor.getString(indexCountry), cursor.getString(indexCity)));
        }
        cursor.close();
        database.close();
        return weathersList;
    }

    /**
     * Перезаписывает таблицу базы данных с данными объектов полученными из листа.
     * <p>Метод используется для изменения порядка в листе. Проверяем не пустой ли лист принимаетмя
     * в качестве параметра. Если не пустой то удаляем все элементы в таблице. И добавляем
     * данные объектов из листа в таблицу, начиная со второго элемента, т.к. первый элемент
     * занят локальным городом.
     * </p>
     *
     * @param weatherList ссылка на локальный лист.
     */

    public void updateList(List<Weather> weatherList) {
        if (weatherList.size() != 0) {
            SQLiteDatabase database = this.getWritableDatabase();
            database.execSQL("DELETE FROM " + DataTable.DB_TABLE);
            for (int i = 1; i < weatherList.size(); i++) {
                Weather weather = weatherList.get(i);
                insertData(weather);
            }
        }
    }
}
