package com.example.weatherapp.db;

import android.database.sqlite.SQLiteDatabase;
/**
 * Класс определяет таблицу с данными.
 */
public class DataTable {

    public static final String DB_TABLE = "citytable";
    public static final String DB_COLUMN_ID = "_id";
    public static final String DB_COLUMN_CITY = "citycolumn";
    public static final String DB_COLUMN_COUNTRY = "countrycolumn";

    /**
     * Метод вызывается при создании базы данных.
     *
     * @param sqLiteDatabase ссылка на базу данных
     */

    public static void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s, %s TEXT NOT NULL);", DB_TABLE, DB_COLUMN_ID, DB_COLUMN_CITY, DB_COLUMN_COUNTRY);
        sqLiteDatabase.execSQL(query);
    }

    /**
     * Метод вызывается при изменении базы данных.
     *
     * @param sqLiteDatabase ссылка на базу данных
     * @param newVersion ссылка на новую версию базы данных
     * @param oldVersion ссылка на старую версию базы данных
     */

    public static void onUpgrade(SQLiteDatabase sqLiteDatabase,
                                 int oldVersion,
                                 int newVersion) {
        String query = String.format("DROP TABLE IF EXISTS %s", DB_TABLE);
        sqLiteDatabase.execSQL(query);
        onCreate(sqLiteDatabase);
    }
}
