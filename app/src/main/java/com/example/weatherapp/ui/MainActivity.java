package com.example.weatherapp.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.weatherapp.R;
import com.example.weatherapp.db.DataHelper;
import com.example.weatherapp.model.Weather;
import com.example.weatherapp.utils.JSONutils;
import com.example.weatherapp.utils.NetworkUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Главная активность приложения.
 * TODO: не очень удачный вариант исполнения, helper не должно быть в основном потоке, сделать
 *  рефакторинг, переработать на архитектурных компоненах гугл...
 */
public class MainActivity
        extends AppCompatActivity
        implements TextToSpeech.OnInitListener {

    private RecyclerView recyclerView;
    private List<Weather> weatherList = new ArrayList<>();
    private MARecyclerAdapter adapter;

    private Weather weather;
    private JSONObject jsonObjectWeather;
    private TextToSpeech textToSpeech;
    private EditText textDialogAdd;

    private String nameCity;
    private LocationManager locationManager;
    private Location location;

    private DataHelper dataHelper;
    private ArrayList<Weather> arrayListFromDb;

    public static final String KEY_MY_PREFS = "KEY_MY_PREFS";
    public static final String KEY_LOCATION_CITY = "KEY_LOCATION_CITY";
    public static final String KEY_LOCATION_COUNTRY = "KEY_LOCATION_COUNTRY";

    private SharedPreferences.Editor editor;

    private static final String EMPTY_REQUEST = "Введи название города, ё моё";
    private static final String WRONG_REQUEST = "Неет такого города, братуха";
    private static final int PERMISSIONS_ACCESS_LOCATION = 333;

    /**
     * Метод, который вызывается при создании нового экземпляра активности. Вызывается один раз,
     * когда происходит инициализация.
     *
     * @param savedInstanceState объект, содержащий сохраненную информацию о состоянии.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
        initDB();
        initPrefs();
        setRecycler();
        if (isConnected()) {
            getLongitudeAndLatitude();
            getFavoriteCities();
        }
        // TODO: добавить вариант отображения без интернета
    }

    /**
     * Метод инициализирует пользовательский интерфейс.
     */
    private void initUI() {
        setContentView(R.layout.ma_activity_main);
        recyclerView = findViewById(R.id.ma_recycler_view);
        textToSpeech = new TextToSpeech(this, this);
    }

    /**
     * Инициализация базы данных.
     */
    private void initDB() {
        dataHelper = new DataHelper(this);
    }

    /**
     * Инициализация базы данных.
     */
    private void initPrefs() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(KEY_MY_PREFS, 0);// 0 - for private mode
        editor = sharedPreferences.edit();
    }

    /**
     * Устанавливаем recyclerView и переопределяет его методы интерфеса для выполнения задач.
     *
     * <p>В методе переопределяются методы интерфеса. Устанавливается адаптер.
     * Устанавливается layout с горизонтальным отображением. Используется snaphelper для
     * привязки firstVisibleItem RecyclerView, firstVisibleItem всегда будет полностью виден,
     * когда прокрутка переходит в положение ожидания.
     * </p>
     */
    private void setRecycler() {
        recyclerView.setHasFixedSize(true);
        adapter = new MARecyclerAdapter(weatherList, new MARecyclerAdapter.ClickListener() {
            @Override
            public void onPositionClickListener(int position) {
                // проверяем не говорит ли сейчас спикер
                if (!textToSpeech.isSpeaking()) {
                    // ставим спикер и получаем позицию
                    speakAboutWeather(position);
                }
            }

            @Override
            public void onLongClickListener(int position) {

            }
        });
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);
    }

    /**
     * Загружает данные о погоде.
     * <p>Получая данные о названии города, обращается к серверу и получает массив JSON с данными
     * о погоде. Далее используя массив JSON получает объект класса weather.  </p>
     *
     * @param nameCity ссылка на название города
     * @return экземпляр класса weather
     */
    private Weather downloadWeather(String nameCity) {
        jsonObjectWeather = NetworkUtils.getJSONFromNetwork(nameCity);
        weather = JSONutils.getWeatherFromJSON(jsonObjectWeather);
        return weather;
        // TODO: добавить вариант использования без интернета
    }

    /**
     * Получает широту и долготу.
     *
     * <p>Использует locationManage. Проверяет мерсию SDK, до 23 то разрешения не требуются.
     * Приложение получило все разрешения при установке. Если версия выше и разрешения нет, то
     * запршивает разрешения. Если разрешения есть то используя Location получает широту и долготу.
     * Полученные данные округляет до целого значения и переводит в строки. Запускает метод
     * downloadWeatherFromLocale();.
     * </p>
     */
    private void getLongitudeAndLatitude() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                        // 1 параметр - массив с разрешенями, в массив можно ложить все необх разрешения
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        // 2 параметр - код запроса, возвращается в метод onRequestPermissionsResult
                        PERMISSIONS_ACCESS_LOCATION
                );
            } else {
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                double longitude = 0;
                double latitude = 0;
                if (location != null) {
                    longitude = location.getLongitude();// может дать налпоинтер
                    latitude = location.getLatitude();
                } else {
                    Toast.makeText(this, "Location error", Toast.LENGTH_SHORT).show();
                }
                int lon = (int) Math.round(longitude);
                int lan = (int) Math.round(latitude);
                String longi = String.valueOf(lon);
                String lati = String.valueOf(lan);
                downloadWeatherFromLocale(longi, lati);
            }
        }
    }

    /**
     * Коллбэк от запроса разрешений на использование данных о местоположении.
     *
     * <p>Возвращает два варианта.</p>
     * <p>Если получено разрешение. Проверяем Location, не является ли пустым значением. Если
     * не является,  то получаем широту и долготу. Если Location возвращает  null, то выводим
     * тост об ошибке. Далее округляет широту и долготу до целого числа и переводит в строку.
     * После чего использует метод downloadWeatherFromLocale(), который загружает погоду используя,
     * полученные параметры.</p>
     * <p>Если разрешение не подучено, то выводит тост об ошибке, сообщает что разрешение не
     * получено.</p>
     */
    // нужные запрошенные разрешения
    // Коллбэк, который выполняется после того, как пользователь выдал или отклонил
    // TODO: разобраться, убрать потеряное разрешение
    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(
            int requestCode, //реквест код
            @NonNull String[] permissions,// массив запросов
            @NonNull int[] grantResults// массив ответов
    ) {
        if (requestCode == PERMISSIONS_ACCESS_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                double longitude = 0;
                double latitude = 0;
                if (location != null) {
                    longitude = location.getLongitude();
                    latitude = location.getLatitude();
                } else {
                    Toast.makeText(this, "Location error", Toast.LENGTH_SHORT).show();
                }
                int lon = (int) Math.round(longitude);
                int lan = (int) Math.round(latitude);
                String longi = String.valueOf(lon);
                String lati = String.valueOf(lan);
                downloadWeatherFromLocale(longi, lati);
            } else {
                // Пользователь не разрешил
                Toast.makeText(
                        this,
                        "Until you grant the permission, we cannot display the names",
                        Toast.LENGTH_SHORT
                ).show();
            }
        }
    }

    /**
     * Загружает данные из сети используя метод getJSONFromNetwork. Далее получает объект класса
     * weather используя данные из массива JSON. Добавляет данные в лист используя метод
     * addWeatherToWeatherList(). После чего использует editor и ложит данные в sharedPreferences.
     *
     * @param longitude ссылка на долготу.
     * @param latitude  ссылка на широту.
     */
    private void downloadWeatherFromLocale(String longitude, String latitude) {
        if (longitude != null && latitude != null) {
            jsonObjectWeather = NetworkUtils.getJSONFromNetwork(longitude, latitude);
            weather = JSONutils.getWeatherFromJSON(jsonObjectWeather);
            addWeatherToWeatherList(weather);
            editor.putString(KEY_LOCATION_CITY, weather.getCityName());
            editor.putString(KEY_LOCATION_COUNTRY, weather.getCountry());
            editor.commit();
        }
    }

    /**
     * Получает лист городов из sqlitedatabase.
     *
     * <p>Если из базы данных возвращается не пустой лист, то перебирает список и создает карточки
     * для recyclerView.</p>
     */
    private void getFavoriteCities() {
        arrayListFromDb = dataHelper.getAllWeathers();
        if (arrayListFromDb.size() != 0) {
            for (int i = 0; i < arrayListFromDb.size(); i++) {
                createCity(arrayListFromDb.get(i).getCityName());
                // для отладки - удаление листа
                // dataHelper.deleteData(arrayListFromDb.get(i).getCityName());
            }
        }
//        // заглушка для теста
//        weatherList.add(new Weather("Ru", "Rybinsk", "12", "8", "cloudy", "http://openweathermap.org/img/wn/10d@2x.png"));
//        weatherList.add(new Weather("Ru", "Moscow", "-22", "-18", "cloudy", "http://openweathermap.org/img/wn/10d@2x.png"));
    }

    // переопределяем метод для произношения голоса

    /**
     * Переопределяет родительский метод.
     *
     * <p>Проверяет статус, не говорит ли сейчас спикер. Если не говорит, то получает
     * дефолтную локаль. Локаль можно поменять на любую другую произвольно, например так
     * Locale locale = new Locale("fr"); Локаль отвечает за язык, которым будет произносится
     * текст. Далее устанавливаем язык. После проверяет поддерживается ли язык. </p>
     *
     * @param status ссылка на переменную статуса.
     */
    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            Locale locale = Locale.getDefault();
            int result = textToSpeech.setLanguage(locale);
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Toast.makeText(this, "Язык не поддерживается", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Ошибка голосового оповещения", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Переопределяет метод TTS, отвечает за отключение TTS.
     */
    @Override
    public void onDestroy() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }

    /**
     * Отвечает за нажатые в активности кнопки.
     *
     * <p>FAB - запускает alert dialog, в котором через edit text можно добавить новый город.</p>
     *
     * <p>Кнопка перехода в активность редактирования, которая запускает intent.</p>
     *
     * @param view ссылка на текущее view.
     */
    public void onClickButton(final View view) {
        switch (view.getId()) {
            case R.id.ma_fab:
                // TODO создать алерт дайлог в котором добавляем возможность ввода города
                addNewCityAlertDialog();
                break;
            case R.id.ma_ib_edit:
                Intent intent = new Intent(this, EAEditActivity.class);
                startActivity(intent);
                break;
        }
    }

    /**
     * Создает новый город и загружает для него погоду методом downloadWeather().
     *
     * @param nameCity ссылка на название города.
     */

    private void createCity(String nameCity) {
        // проверка на пустой стринг и не содержит лист такой город
        if (!TextUtils.isEmpty(nameCity)) {
            // проверяем есть ли интернет и пытаемся загрузить по названию джейсон
            if (isConnected()) {
                downloadWeather(nameCity);
            } else {
                Toast.makeText(this, "Ошибка, нет интернета", Toast.LENGTH_SHORT).show();
            }
            // проверяем возвращается ли название города, если да то названачаем поля
            if (!weatherList.contains(downloadWeather(nameCity))) {
                // добавляем полученные из джейсона данные в лист
                addWeatherToWeatherList(weather);
                // сообщаем адаптеру что данные изменились
                adapter.notifyDataSetChanged();
            } else {
                // в алерт дайлоге нет такого вью
                textToSpeech.speak(WRONG_REQUEST, TextToSpeech.QUEUE_ADD, null, null);
                Toast.makeText(this, "Нет такого города", Toast.LENGTH_SHORT).show();
            }
        } else {
            // в алерт дайлоге нет такого вью
            textToSpeech.speak(EMPTY_REQUEST, TextToSpeech.QUEUE_ADD, null, null);
            Toast.makeText(this, "Нет такого города", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Добавляет данные о погоде в лист.
     *
     * @param weather ссылка на объект погоды.
     */
    private void addWeatherToWeatherList(Weather weather) {
        weatherList.add(new Weather(
                weather.getCountry(),
                weather.getCityName(),
                weather.getTemp(),
                weather.getTempFeelsLike(),
                weather.getWeatherDescription(),
                weather.getPicture()));
    }

    /**
     * Создает alert dialog для добавления нового города.
     * TODO: нужно заменить на фрагмент диалог, т.к. при повороте экрана алерт пропадает
     */
    private void addNewCityAlertDialog() {
        // Для создания кастомного диалога нужно создать Layout XML файл
        // "Надуваем" view диалога используя xml файл
        View dialog = getLayoutInflater().inflate(R.layout.ma_add_city_dialog, null);
        textDialogAdd = dialog.findViewById(R.id.dialog_add);
        // Находим EditText используя только что "надутый" view
        // Создаем диалог используя шаблон Builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialog)
                .setTitle("Добавьте город")
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton("Добавить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        nameCity = textDialogAdd.getText().toString();
                        // TODO добавить сюда проверку до закрытия окна

                        // создаем город и добавляем в лист
                        createCity(nameCity);

                        if (!TextUtils.isEmpty(downloadWeather(nameCity).getCityName())) {
                            // прокручиваем на послденюю позицию, т.е. позицию добавленного города
                            recyclerView.smoothScrollToPosition(weatherList.size());
                            if (!weatherList.contains(downloadWeather(nameCity))) {
                                // добавляем данные в базу, если такого города в базе еще нет
                                insertWeatherToDB(downloadWeather(nameCity));
                            }
                        }
                        // Закрываем диалог в положительном ключе
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    /**
     * Добавляет объект погоды в базу данных.
     *
     * @param weather ссылка на объект погоды.
     */
    private void insertWeatherToDB(Weather weather) {
        // добавляем данные в базу
        // проверяем есть ли в базе такое имя и возвращает ли название города json
        if (!arrayListFromDb.contains(weather) && !TextUtils.isEmpty(weather.getCityName())) {
            dataHelper.insertData(weather);
        }
    }

    /**
     * Запускает спикер и озвучивает погоду.
     *
     * @param position ссылка на текущую позицию.
     */
    private void speakAboutWeather(int position) {
        String weatherToString = String.format("В городе %s сегодня: \n%s\nтемпература %s\nощущается как %s",
                weatherList.get(position).getCityName(),
                weatherList.get(position).getWeatherDescription(),
                weatherList.get(position).getTemp(),
                weatherList.get(position).getTempFeelsLike());
        // передаем текст в спич
        textToSpeech.speak(weatherToString,
                //TextToSpeech.QUEUE_FLUSH, // флаг который обрывает все начатые фразы и начинает новую
                TextToSpeech.QUEUE_ADD, // флаг который ставит в очередь начатые фразы
                null,
                null);
    }

    /**
     * Проверяет есть ли подключение к интернету.
     *
     * @return boolean подключение к интернету.
     */
    public boolean isConnected() {
        // TODO: добавить слушателя подключения к интернет
        //  и создать зависимости
        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    private void __printLOG(String s) {
        Log.d("LOG_TAG", s);
    }

    private void __printLOG(Object o) {
        Log.d("LOG_TAG", String.valueOf(o));
    }
}
