package com.example.weatherapp.ui;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherapp.R;
import com.example.weatherapp.model.Weather;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Класс наследующий базовый класс адаптер.
 */
public class MARecyclerAdapter
        extends RecyclerView.Adapter<MARecyclerAdapter.PageViewHolder> {

    private List<Weather> weathers;
    private ClickListener clickListener;

    private String symbolTemp;
    private String symbolTempFeels;
    private String celsius = "\u00B0" + "C";

    /**
     * Интерфейс для получения позиции активностью.
     */
    public interface ClickListener {
        void onPositionClickListener(int position);

        void onLongClickListener(int position);
    }

    /**
     * Конструктор принимающий в параметр List и объект интерфейса для слушания позиции.
     *
     * @param weatherList   ссылка на лист
     * @param clickListener ссылка на объект интерфейса
     */
    // создаем конструктор адаптера и добавляем туда лист и слушателя для получения позиций
    public MARecyclerAdapter(List<Weather> weatherList, ClickListener clickListener) {
        this.weathers = weatherList;
        this.clickListener = clickListener;
    }

    /**
     * Переопределяет родительский метод.
     *
     * <p>В методе создает надувателя. Надувает item. </p>
     *
     * @param parent   ссылка на родителя.
     * @param viewType ссылка на тип view.
     * @return новый  ViewHolderEA с параметрами view  и слушателя позиций.
     */
    @NonNull
    @Override
    public PageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.ma_page, parent, false);
        return new PageViewHolder(v, clickListener);
    }

    /**
     * Переопределяет родительский метод.
     *
     * <p>Получает из листа элемент класса Weather по позиции. Устанавливает холдер на полученный
     * элемент. Проверяет если полученный элемент первый в списке то делает на нем видимым
     * imageView - локация.  </p>
     *
     * @param holder   ссылка на холдер
     * @param position ссылка на позицию
     */
    @Override
    public void onBindViewHolder(@NonNull final PageViewHolder holder, int position) {
        Weather w = weathers.get(position);
        holder.setWeather(w);
    }

    /**
     * Метод возвращает тип элемента по позиции.
     *
     * @param position ссылка на позицию.
     * @return возвращает метод родителя.
     */
    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    /**
     * Метод определяет количество элементов в списке.
     *
     * @return количество элементов в листе.
     */
    @Override
    public int getItemCount() {
        return weathers.size();
    }


    /**
     * Класс пользовательского холдера.
     */
    class PageViewHolder
            extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private TextView city;
        private TextView temp;
        private TextView tempLikeDesc;
        private TextView tempLike;
        private TextView description;
        private ImageView imageViewDescription;
        private RelativeLayout imageViewBackground;

        private Context context;

        /**
         * Конструктор пользовательского холдера.
         *
         * <p>В конструкторе получаем контекст itemView. Инициализирует элементы itemView.
         * </p>
         *
         * @param itemView      ссылка на view элемента.
         * @param clickListener ссылка на слушателя позиций.
         */
        public PageViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);

            this.context = itemView.getContext();

            city = itemView.findViewById(R.id.page_tv_city);
            temp = itemView.findViewById(R.id.page_tv_temperature);
            tempLikeDesc = itemView.findViewById(R.id.page_tv_temp_feels_like_desc);
            tempLike = itemView.findViewById(R.id.page_tv_temp_feels_like);
            description = itemView.findViewById(R.id.page_tv_description);
            imageViewDescription = itemView.findViewById(R.id.page_iv_description);
            imageViewBackground = itemView.findViewById(R.id.imageViewBackground);
            ImageButton imageButtonSpeak = itemView.findViewById(R.id.page_ib_speak);

            imageButtonSpeak.setOnClickListener(this);
            // проверяем версию, если ниже 22 то убираем возможность
            // говорить, т.к. она не поддерживается
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                imageButtonSpeak.setVisibility(View.INVISIBLE);
            }
            setBackground();
        }


        /**
         * Устанавливает значения полей в элементе.
         *
         * <p>Использует текстовое поле и для установки изображений из интернет библиотеку Picasso</p>
         *
         * @param w ссылка на объект.
         */
        public void setWeather(Weather w) {
            Picasso.get()
                    .load(w.getPicture())
                    .fit()
                    .placeholder(R.drawable.dummy) // preload
                    .error(R.drawable.dummy) // load error
                    .centerCrop()
                    .into(imageViewDescription, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            e.printStackTrace();
                            e.getCause();
                            Toast.makeText(context, "Ошибка загрузки изображения флага",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });

            if (Integer.parseInt(w.getTemp()) > 0) {
                symbolTemp = "+";
            } else if (Integer.parseInt(w.getTemp()) < 0) {
                symbolTemp = "";
            }
            if (Integer.parseInt(w.getTempFeelsLike()) > 0) {
                symbolTempFeels = "+";
            } else if (Integer.parseInt(w.getTempFeelsLike()) < 0) {
                symbolTempFeels = "";
            }
            String tempFromJson = w.getTemp();
            String tempFormat = String.format("%s%s%s", symbolTemp, tempFromJson, celsius);
            String tempFeelsFromJson = w.getTempFeelsLike();
            String tempFeelsFormat = String.format("%s%s%s", symbolTempFeels, tempFeelsFromJson, celsius);
            city.setText(w.getCityName());
            temp.setText(tempFormat);
            tempLikeDesc.setText(R.string.feels_like_decs);
            tempLike.setText(tempFeelsFormat);
            description.setText(w.getWeatherDescription());

        }

        /**
         * Обработчик нажатий на кнопки в элементе
         *
         * @param v ссылка на view элемента
         */
        @Override
        public void onClick(View v) {
//            if (v.getId() == imageButtonSpeak.getId()) {
//                Toast.makeText(context, getAdapterPosition() + "", Toast.LENGTH_SHORT).show();
//            }
            clickListener.onPositionClickListener(getAdapterPosition());
        }

        /**
         * Устанавливает задний фон.
         *
         * <p>Получает текущую ориентацию. Устанавливает задний в зависимости от ориентации.</p>
         */
        private void setBackground() {
            int orientation = context.getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                imageViewBackground.setBackgroundResource(R.drawable.background_blue_sky);
            } else {
                imageViewBackground.setBackgroundResource(R.drawable.background_blue_sky_land);
            }
            //TODO: можно добавить темную тему в зависимости от времени суток,
            // можно добавить фон в зависимости от погоды

        }
    }


}
