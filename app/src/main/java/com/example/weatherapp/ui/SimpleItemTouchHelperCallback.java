package com.example.weatherapp.ui;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Класс наследник ItemTouchHelper, переопределяет методы onMove() и onSwiped() для
 * перетаскивания капли и смахивания с экрана
 */
public class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {


    private final ItemTouchHelperAdapter mAdapter;

    /**
     * Конструктор с параметром объекта интерфеса.
     *
     * @param adapter ссылка на адаптер интерфейса.
     */
    public SimpleItemTouchHelperCallback(ItemTouchHelperAdapter adapter) {
        mAdapter = adapter;
    }

    /**
     * Возвращает boolean если лонг пресс.
     *
     * @return boolean.
     */
    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    /**
     * Возвращает boolean если свайп.
     *
     * @return boolean.
     */
    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }

    /**
     * Возвращает разрешенные движения свайпов.
     *
     * @param recyclerView ссылка на recyclerView.
     * @param viewHolder   ссылка на viewHolder.
     * @return int разрешения.
     */
    @Override
    public int getMovementFlags(RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder) {
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    /**
     * Переопределяет метод для перетаскивания капли.
     *
     * @param recyclerView ссылка на recyclerView.
     * @param viewHolder   ссылка на viewHolder.
     * @param target       ссылка на target.
     * @return boolean.
     */
    @Override
    public boolean onMove(RecyclerView recyclerView,
                          RecyclerView.ViewHolder viewHolder,
                          RecyclerView.ViewHolder target) {
        mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    /**
     * Переопределяет метод для смахивания.
     *
     * @param viewHolder ссылка на viewHolder.
     * @param direction  ссыдка на направление.
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        mAdapter.onItemDismiss(viewHolder.getAdapterPosition());
    }
}