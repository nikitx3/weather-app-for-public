package com.example.weatherapp.ui;

/**
 * Интерфейс для работы swipe to dismiss и drag & drop.
 *
 * <p>в интерфейсе два метода: onItemMove - для перетаскивания капли и onItemDismiss - для
 * смахивания элемента с экрана. </p>
 */
public interface ItemTouchHelperAdapter {

    boolean onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}