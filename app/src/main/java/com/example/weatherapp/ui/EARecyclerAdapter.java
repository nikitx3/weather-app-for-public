package com.example.weatherapp.ui;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherapp.R;
import com.example.weatherapp.model.Weather;
import com.example.weatherapp.utils.NetworkUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

/**
 * Класс наследующий базовый класс адаптера и расширенный  интерфейсом ItemTouchHelperAdapter.
 */

public class EARecyclerAdapter
        extends RecyclerView.Adapter<EARecyclerAdapter.ViewHolderEA>
        implements ItemTouchHelperAdapter {

    private List<Weather> weatherList;
    private final PositionListener positionListener;
    private ImageView imageViewCurrentLocation;

    /**
     * Конструктор адаптера, котоый принимает в качестве параметров лист и слушателя для
     * получения позиций.
     *
     * @param weatherList      ссылка на лист с объектами weather.
     * @param positionListener ссылка на объект слушателя позиций.
     */
    public EARecyclerAdapter(List<Weather> weatherList, PositionListener positionListener) {
        this.weatherList = weatherList;
        this.positionListener = positionListener;
    }

    /**
     * Переопределяет родительский метод.
     *
     * <p>В методе создает надувателя. Надувает item. </p>
     *
     * @param parent   ссылка на родителя.
     * @param viewType ссылка на тип view.
     * @return новый  ViewHolderEA с параметрами view  и слушателя позиций.
     */
    @NonNull
    @Override
    public ViewHolderEA onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.ea_item_edit_ativity, parent, false);
        return new ViewHolderEA(v, positionListener);
    }

    /**
     * Переопределяет родительский метод.
     *
     * <p>Получает из листа элемент класса Weather по позиции. Устанавливает холдер на полученный
     * элемент. Проверяет если полученный элемент первый в списке то делает на нем видимым
     * imageView - локация.  </p>
     *
     * @param holder   ссылка на холдер
     * @param position ссылка на позицию
     */
    @Override
    public void onBindViewHolder(@NonNull final ViewHolderEA holder, final int position) {
        Weather w = weatherList.get(position);
        holder.setItemFields(w);
        if (position == 0) {
            // TODO: добавить запрет на удаление города по локации
            imageViewCurrentLocation.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Метод возвращает тип элемента по позиции.
     *
     * @param position ссылка на позицию.
     * @return возвращает метод родителя.
     */
    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    /**
     * Метод определяет количество элементов в списке.
     *
     * @return количество элементов в листе.
     */
    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    /**
     * Метод для перемещения капли.
     *
     * <p>Проверяет какая из позиций в параметре больше. В зависимости от параметров меняет
     * позиции местами.</p>
     *
     * @param fromPosition ссылка на начальную позицию.
     * @param toPosition   ссылка на конечную позицию.
     * @return true.
     */
    // переопределяем метод для перемещения капли
    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(weatherList, i, i + 1);
                positionListener.onMoveClickListener(fromPosition, toPosition);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(weatherList, i, i - 1);
                positionListener.onMoveClickListener(fromPosition, toPosition);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    /**
     * Удаляет элемент по позиции из листа при смахивании с экрана.
     *
     * @param position ссылка на позицию.
     */
    @Override
    public void onItemDismiss(int position) {
        positionListener.onDismissClickListener(position);
        weatherList.remove(position);
        notifyItemRemoved(position);
    }

    /**
     * Класс пользовательского холдера.
     */

    class ViewHolderEA
            extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private TextView textViewCity;
        private ImageView imageViewFlag;
        private ImageButton imageButtonSave;
        private Context context;

        /**
         * Конструктор пользовательского холдера.
         *
         * <p>В конструкторе получаем контекст itemView. Инициализирует элементы itemView.
         * </p>
         *
         * @param itemView         ссылка на view элемента.
         * @param positionListener ссылка на слушателя позиций.
         */
        public ViewHolderEA(final View itemView, PositionListener positionListener) {
            super(itemView);
            this.context = itemView.getContext();
            textViewCity = itemView.findViewById(R.id.item_tv_city);
            imageViewFlag = itemView.findViewById(R.id.item_iv_flag);
            imageViewCurrentLocation = itemView.findViewById(R.id.item_iv_location_badge);

        }


        /**
         * Устанавливает значения полей в элементе.
         *
         * <p>Использует текстовое поле и для установки изображений из интернет библиотеку Picasso</p>
         *
         * @param w ссылка на объект.
         */
        public void setItemFields(Weather w) {
            textViewCity.setText(w.getCityName());

            Picasso.get()
                    .load(NetworkUtils.getFlagIcon(w.getCountry()))
                    .fit()
                    .placeholder(R.drawable.ic_update_gray_48dp) // preload
                    .error(R.drawable.ic_update_gray_48dp) // load error
                    .centerCrop()
                    .into(imageViewFlag, new Callback() {//ставим слушателя ошибок пикассо
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            Toast.makeText(context, "Ошибка загрузки изображения флага",
                                    Toast.LENGTH_SHORT).show();
                            Log.d("LOG_TAG", e.toString());
                        }
                    });
        }

        /**
         * Обработчик нажатий на кнопки в элементе
         *
         * @param v ссылка на view элемента
         */
        @Override
        public void onClick(View v) {
//            if (v.getId() == deleteButton.getId()) {
//                View parent = (View) v.getParent();
//                parent.animate().alpha(0).setDuration(2000).withEndAction(new Runnable() {
//                    @Override
//                    public void run() {
//                        // передаем данные о позиции в активность
//            positionListener.onPositionClickListener(getAdapterPosition());
//                        //удаляем элемент из вью
//                        weatherList.remove(getAdapterPosition());
//                        //сообщаем адаптеру, что надо перестроить вью
//                        notifyItemRemoved(getAdapterPosition());
//                    }
//                });
//            }

        }
    }
}
