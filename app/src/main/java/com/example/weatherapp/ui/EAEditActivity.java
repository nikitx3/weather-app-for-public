package com.example.weatherapp.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.example.weatherapp.R;
import com.example.weatherapp.db.DataHelper;
import com.example.weatherapp.model.Weather;

import java.util.ArrayList;
import java.util.List;

/**
 * Активность для редактирования.
 * TODO: не очень удачный вариант исполнения, helper не должно быть в основном потоке, сделать
 *  рефакторинг, переработать...
 */

public class EAEditActivity
        extends AppCompatActivity {

    private ImageButton imageButtonBack;
    private ImageButton imageButtonSave;
    private RecyclerView recyclerView;

    private List<Weather> weatherList = new ArrayList<>();

    private EARecyclerAdapter recyclerAdapter;

    private DataHelper dataHelper;
    private ArrayList<Weather> arrayListWeathers;
    private String cityName;
    private String country;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;


    /**
     * Метод, который вызывается при создании нового экземпляра активности. Вызывается один раз,
     * когда происходит инициализация.
     *
     * @param savedInstanceState объект, содержащий сохраненную информацию о состоянии.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initUI();
        initDB();
        setRecycler();
        getCityFromLocate();
        getList();
        getSwipeToDismissAndDragDrop();
    }

    /**
     * Метод инициализирует пользовательский интерфейс.
     */
    private void initUI() {
        setContentView(R.layout.ea_activity_edit);
        imageButtonBack = findViewById(R.id.ea_ib_back);
        imageButtonSave = findViewById(R.id.ea_ib_save);
        recyclerView = findViewById(R.id.ea_recycler_view);
    }

    /**
     * Инициализация базы данных.
     */
    private void initDB() {
        dataHelper = new DataHelper(this);
        arrayListWeathers = dataHelper.getAllWeathers();
    }

    /**
     * Устанавливаем recyclerView и переопределяет его методы интерфеса для выполнения задач.
     *
     * <p>В методе переопределяются методы интерфеса. Устанавливается адаптер.
     * Устанавливается layout с вертикальным отображением. Используется snaphelper для
     * привязки firstVisibleItem RecyclerView, firstVisibleItem всегда будет полностью виден,
     * когда прокрутка переходит в положение ожидания.
     * </p>
     */
    private void setRecycler() {
        recyclerView.setHasFixedSize(true);

        //
        recyclerAdapter = new EARecyclerAdapter(weatherList, new PositionListener() {
            // добавляем в метод интерфейса получение позиции на клик

            /**
             * Переопределяем метод интерфейса для слушателя позиций по клику.
             *
             * <p>В методе получая позицию удаляем город по клику на кнопку удаления.
             * </p>
             *
             * @param position ссылка на позицию.
             */
            @Override
            public void onPositionClickListener(int position) {
                deleteCity(position);
            }

            /**
             * Переопределяем метод интерфейса для слушателя позиций по длинному клику.
             *
             * @param position ссылка на позицию.
             */
            @Override
            public void onLongClickListener(int position) {
            }

            /**
             * Переопределяем метод интерфейса для слушателя позиций для перетаскивания капли.
             *
             * @param fromPosition ссылка на позицию с которой перетаскиватся капля.
             * @param toPosition ссылка на позицию на которую перетаскиватся капля.
             */
            @Override
            public void onMoveClickListener(int fromPosition, int toPosition) {
                imageButtonSave.animate().alpha(1f).setDuration(500);
            }

            /**
             * Переопределяем метод интерфейса для слушателя позиций для смахивания
             * элементов с экрана.
             *
             * <p>Метод используется для удаления используя текущую позицию</p>
             *
             * @param position ссылка на позицию.
             */
            @Override
            public void onDismissClickListener(int position) {
                // TODO: настроить удаление по смаху вправо и добавление анимации
                deleteCity(position);
                imageButtonSave.animate().alpha(1f).setDuration(500);
            }
        });

        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);
    }

    /**
     * Получает город текущей локации из базы данных приложения.
     *
     * <p>Метод использует класс sharedPreferences, получая данные по ключу KEY_MY_PREFS.
     * Присваивает ззначение города и страны строковым переменным. Проверяет не пустой ли параметр
     * с названием города. Если не пустой то добавлет его в лист.      *
     * </p>
     */
    private void getCityFromLocate() {
        sharedPreferences = getApplicationContext().getSharedPreferences(MainActivity.KEY_MY_PREFS, 0);
        String cityName = sharedPreferences.getString(MainActivity.KEY_LOCATION_CITY, "");
        String country = sharedPreferences.getString(MainActivity.KEY_LOCATION_COUNTRY, "");
        if (!TextUtils.isEmpty(cityName)) {
            weatherList.add(new Weather(
                    country,
                    cityName));
            recyclerAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Получает лист городов и стран для отображения в списке recyclerView.
     *
     * <p>Метод проверяет есть ли элементы в списке возвращенном из базы данных.
     * Если список не пустой, то начинает перебирать элементы, получать из них строковые ресурсы
     * названий города и страны и добавляет их лист для recyclerView. По окончанию уведомляет
     * адаптер о том, что данные изменились.
     * </p>
     */
    private void getList() {
        if (arrayListWeathers.size() != 0) {
            // перебираем элементы листа и добавляем город и страну в лист
            for (int i = 0; i < arrayListWeathers.size(); i++) {
                String cityName = arrayListWeathers.get(i).getCityName();
                String country = arrayListWeathers.get(i).getCountry();

                weatherList.add(new Weather(
                        country,
                        cityName));
            }
            recyclerAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Удаляет строку из базы данных по названию города.
     *
     * @param position ссылка на позицию элемента
     */
    private void deleteCity(int position) {
        dataHelper.deleteData(weatherList.get(position).getCityName());
    }

    /**
     * Обновляет лист с погодой.
     *
     * @param weatherList лист содержащий объекты weather
     */
    private void updateList(List<Weather> weatherList) {
        dataHelper.updateList(weatherList);
    }

    /**
     * Метод слушает нажатия на кнопки в активности.
     *
     * <p>
     * В методе используется две кнопки.
     * </p>
     * <p>      В первом случае кнопка назад, которая возвращает на главную активность</p>
     * <p>Во втором случае кнопка сохранить, которая использует метод обновить лист и перезаписывает
     * данные в базу данных.</p>
     *
     * @param view ссылка на текущее view.
     */
    public void onButtonClick(View view) {
        switch (view.getId()) {
            case R.id.ea_ib_back:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.ea_ib_save:
                imageButtonSave.animate().alpha(0.5f).setDuration(500);
                updateList(weatherList);
        }
    }

    /**
     * Метод подключает ItemTouchHelper, для использования Swipe to dismiss, а также Drag & Drop.
     */
    private void getSwipeToDismissAndDragDrop() {
        ItemTouchHelper.Callback callback =
                new SimpleItemTouchHelperCallback(recyclerAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);
    }
}