package com.example.weatherapp.ui;

public interface PositionListener {

    void onPositionClickListener(int position);

    void onLongClickListener(int position);

    void onMoveClickListener(int fromPosition, int toPosition);

    void onDismissClickListener(int position);
}