package com.example.weatherapp.utils;

import android.net.Uri;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class NetworkUtils {

    private static final String BASE_URL_CITY = "https://api.openweathermap.org/data/2.5/weather?q=";
    private static final String BASE_URL_LONLAT = "https://api.openweathermap.org/data/2.5/weather?lat=";
    private static final String BASE_API_KEY = "8a832c01210943d21ff1495629d9d6c4";
    private static final String BASE_API_COUNTRY = "rus";
    private static final String BASE_API_UNITS = "metric";
    private static final String BASE_API_LANG = "ru";

    // api.openweathermap.org/data/2.5/weather?q={city name}&appid={your api key}

    /**
     * Создает строку из параметров для запроса json.
     *
     * @param cityName ссылка на название города.
     * @return URL ссылку на json.
     */
    public static URL buildUrl(String cityName) {
        URL result = null;
        Uri uri = Uri.parse(String.format(BASE_URL_CITY + cityName + "&appid=" + BASE_API_KEY + "&lang=" + BASE_API_LANG + "&units=" + BASE_API_UNITS)).buildUpon().build();
        try {
            result = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return result;
    }

    // api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={your api key}
    // https://samples.openweathermap.org/data/2.5/weather?lat=35&lon=139&appid=b6907d289e10d714a6e88b30761fae22

    /**
     * Создает строку из параметров для запроса json.
     *
     * @param lat ссылка на широту.
     * @param lon ссылка на долготу.
     * @return URL ссылку на json.
     */
    public static URL buildUrl(String lon, String lat) {
        URL result = null;
        Uri uri = Uri.parse(String.format(BASE_URL_LONLAT + lat + "&lon=" + lon + "&appid=" + BASE_API_KEY + "&lang=" + BASE_API_LANG + "&units=" + BASE_API_UNITS)).buildUpon().build();
        try {
            result = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Получает json object
     *
     * @param cityName ссылка на название города.
     * @return json object.
     */
    public static JSONObject getJSONFromNetwork(String cityName) {
        JSONObject result = null;
        URL url = buildUrl(cityName);
        try {
            result = new DownloadJSONTask().execute(url).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Получает json object
     *
     * @param lon ссылка на долготу.
     * @param lat ссылка на широту.
     * @return json object.
     */
    public static JSONObject getJSONFromNetwork(String lon, String lat) {
        JSONObject result = null;
        URL url = buildUrl(lon, lat);
        try {
            result = new DownloadJSONTask().execute(url).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Класс создающий асинхронную задачу для загрузки данных
     */
    private static class DownloadJSONTask extends AsyncTask<URL, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(URL... urls) {
            JSONObject result = null;
            if (urls == null || urls.length == 0) {
                return null;
            }
            HttpURLConnection urlConnection = null;
            try {
                urlConnection = (HttpURLConnection) urls[0].openConnection();
                InputStream is = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(is);
                BufferedReader bufferedReader = new BufferedReader(reader);
                StringBuilder builder = new StringBuilder();
                String line = bufferedReader.readLine();
                while (line != null) {
                    builder.append(line);
                    line = bufferedReader.readLine();
                }
                result = new JSONObject(builder.toString());
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return result;
        }
    }

    /**
     * Получает ссылку для получения изображения флага.
     * <p>Изменяя параметры можно получить простой -  flat, или объемный - shiny фдаг. Также
     * можно изменить размер полаемого изображения флага 16-24-32-48-64 px.
     * </p>
     *
     * @param country ссылка на код страны.
     * @return String ссылка на изображение флага.
     */
    public static String getFlagIcon(String country) {
        return String.format("https://www.countryflags.io/%s/%s/%s.png", country, "flat", "64");
    }
}
