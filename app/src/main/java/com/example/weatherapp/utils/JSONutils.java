package com.example.weatherapp.utils;

import android.text.TextUtils;
import android.util.Log;

import com.example.weatherapp.model.Weather;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Класс для получения данных из JSON.
 */
public class JSONutils {

    private static final String KEY_CITY_NAME = "name";

    private static final String KEY_COORD = "coord";
    private static final String KEY_COORD_LON = "lon";
    private static final String KEY_COORD_LAT = "lat";

    private static final String KEY_MAIN = "main";
    private static final String KEY_MAIN_TEMP = "temp";
    private static final String KEY_MAIN_FEELS_LIKE = "feels_like";
    private static final String KEY_MAIN_PRESSURE = "pressure";
    private static final String KEY_MAIN_HUMIDITY = "humidity";

    private static final String KEY_WEATHER = "weather";
    private static final String KEY_WEATHER_MAIN = "main";
    private static final String KEY_WEATHER_DESCRIPTION = "description";
    private static final String KEY_WEATHER_ICON = "icon";

    private static final String KEY_SYS = "sys";
    private static final String KEY_SYS_COUNTRY = "country";
    private static final String KEY_SYS_SUNRISE = "sunrise";
    private static final String KEY_SYS_SUNSET = "sunset";

    private static final String KEY_ICON = "icon";

    //http://openweathermap.org/img/wn/10d@2x.png
    //https://openweathermap.org/weather-conditions
    private static final String URL_PIC = "http://openweathermap.org/img/wn/%s@2x.png";

    // {"coord":{"lon":38.84,"lat":58.04},"weather":[{"id":804,"main":"Clouds","description":"пасмурно","icon":"04d"}],"base":"stations","main":{"temp":1.65,"feels_like":-4.22,"temp_min":1.65,"temp_max":1.65,"pressure":1018,"humidity":84,"sea_level":1018,"grnd_level":1002},"wind":{"speed":5.4,"deg":233},"clouds":{"all":92},"dt":1584508887,"sys":{"country":"RU","sunrise":1584502294,"sunset":1584545623},"timezone":10800,"id":500004,"name":"Рыбинск","cod":200}

    /**
     * Возвращает погоду полученную из JSON.
     *
     * @param jsonObject ссылка на объект json.
     * @return объект погоды.
     */
    public static Weather getWeatherFromJSON(JSONObject jsonObject) {
        //Log.d("LOG_TAG", jsonObject.toString());
        Weather weatherResult = null;
        if (jsonObject == null) {
            return new Weather();
        }
        String nameCity;
        try {
            nameCity = jsonObject.getString(KEY_CITY_NAME);
            // получаем данные из объекта main
            JSONObject main = jsonObject.getJSONObject(KEY_MAIN);
            String temp = main.getString(KEY_MAIN_TEMP);
            String tempFeelsLike = main.getString(KEY_MAIN_FEELS_LIKE);
            //получаем объект из массива данных weather
            JSONArray jsonArray = jsonObject.getJSONArray(KEY_WEATHER);
            JSONObject weather = jsonArray.getJSONObject(0);
            String mainWeather = weather.getString(KEY_WEATHER_MAIN);
            String description = weather.getString(KEY_WEATHER_DESCRIPTION);
            String iconCod = weather.getString(KEY_ICON);
            // получаем код страны из sys
            JSONObject sys = jsonObject.getJSONObject(KEY_SYS);
            String countryCod = sys.getString(KEY_SYS_COUNTRY);

            // форматируем под сслыку icon
            String icon = String.format(URL_PIC, iconCod);

            if (!TextUtils.isEmpty(nameCity)) {
                weatherResult = new Weather(countryCod, nameCity, temp, tempFeelsLike, description, icon);
            } else {
                Log.d("LOG_TAG", "getWeatherFromJSON -name city is empty, network error-");
            }
            return weatherResult;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return weatherResult;
    }
}
