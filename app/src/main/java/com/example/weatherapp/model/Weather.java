package com.example.weatherapp.model;

/**
 * Класс определяет объект погоды.
 */

public class Weather {

    private int cityId;
    private String cityName;

    private float lon;
    private float lat;

    private String temp;
    private String tempFeelsLike;
    private String pressure;
    private String humidity;

    private String weatherMain;
    private String weatherDescription;
    private String weatherIcon;

    private String country;
    private String sunrise;
    private String sunset;

    private String picture;

    /**
     * Пустой конструктор.
     */
    public Weather() {
    }

    /**
     * Конструктор для главной активности.
     */
    public Weather(String country, String cityName, String temp, String tempFeelsLike, String weatherDescription, String picture) {
        this.country = country;
        this.cityName = cityName;
        this.temp = temp;
        this.tempFeelsLike = tempFeelsLike;
        this.weatherDescription = weatherDescription;
        this.picture = picture;
    }

    /**
     * Конструктор для активности редактирования.
     */

    public Weather(String country, String cityName) {
        this.cityName = cityName;
        this.country = country;
    }

    /**
     * Getter для получения названия города.
     *
     * @return возвращает город.
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * Getter для получения температуры.
     * <p>Метод использует температуру, округляя ее до целого числа и возвращает в формате строки.
     * </p>
     *
     * @return возвращает текущую температуру.
     */
    public String getTemp() {
        int result = (int) Math.round(Double.parseDouble(temp));
        temp = String.valueOf(result);
        return temp;
    }

    /**
     * Getter для получения температуры.
     * <p>Метод использует ощущаемую температуру, округляя ее до целого числа и возвращает
     * в формате строки.
     * </p>
     *
     * @return возвращает ощущаемую температуру.
     */
    public String getTempFeelsLike() {
        int result = (int) Math.round(Double.parseDouble(tempFeelsLike));
        tempFeelsLike = String.valueOf(result);
        return tempFeelsLike;
    }

    /**
     * Getter для получения описания погоды.
     *
     * @return возвращает описание погоды.
     */
    public String getWeatherDescription() {
        return weatherDescription;
    }

    /**
     * Getter для получения ссылки на изображение описания погоды.
     *
     * @return возвращает строку со ссылкой на описание погоды.
     */
    public String getPicture() {
        return picture;
    }

    /**
     * Getter для получения кода страны.
     *
     * @return возвращает код страны.
     */
    public String getCountry() {
        return country;
    }
}
